#include <iostream>
#include <vector>
using namespace std;

int main() {
	int first_term = 1;
	int second_term = 2;
	int total = 0;
	
	while (second_term + first_term < 4000000) {
		int temp_first_term = first_term;
		int temp_second_term = second_term;
		first_term = temp_first_term + temp_second_term;
		if (first_term % 2 == 0) {
			total = first_term + total;
		}
		second_term = first_term + temp_second_term;
		if (second_term % 2 == 0) {
			total = second_term + total;
		}
		
	}
	cout << total + 2 << endl;
	
}
